// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include <QTest>

#include "geminidocument.h"

class Document: public QObject
{
    Q_OBJECT
private slots:
  void testFormat();

    void testPartial_data();
    void testPartial();

    void testLineEndings();
};

void Document::testFormat()
{
  const QUrl url{"file://foo"};

  const QString source = "foo\nbar";
  // no newline for last line since toPlainText() does not generate one

  GeminiDocument document;
  document.setSource(source, url);

  QCOMPARE(document.source(), source);
  QCOMPARE(document.toPlainText(), source);
}

void Document::testPartial_data()
{
  QTest::addColumn<QStringList>("sourceParts");

  QTest::newRow("at linebreak") << QStringList({"asdf\n", "bar"});
  QTest::newRow("over linebreak") << QStringList({"asdf", "ba\nr\n"});
  QTest::newRow("split headings") << QStringList({"# FOO\n", "#", "#", "# bar\n"});
  QTest::newRow("split list") << QStringList({"foo b", "ar\n* ", "a\n* b\n", "* c\n"});
}

void Document::testPartial()
{
  const QUrl url{"file://foo"};

  GeminiDocument document;

  document.setUrl(QUrl("file://foo"));

  QFETCH(QStringList, sourceParts);

  QString source;
  for(int i=0; i<sourceParts.size(); i++) {
    source.append(sourceParts[i]);
    document.appendSource(sourceParts[i]);
  }

  QCOMPARE(document.source(), source);

  GeminiDocument doc2;
  doc2.setSource(source, url);
  QCOMPARE(doc2.source(), source);
  QCOMPARE(document.toPlainText(), doc2.toPlainText());
  QCOMPARE(document.toHtml(), doc2.toHtml());
}

void Document::testLineEndings()
{
  GeminiDocument docUnix;
  GeminiDocument docDos;

  const QUrl url{"file://foo"};
  docUnix.setSource("foo\nbar\nbaz\n", url);
  docDos.setSource("foo\r\nbar\r\nbaz\r\n", url);

  QCOMPARE(docDos.toPlainText(), docUnix.toPlainText());

  GeminiDocument docMixed;
  docMixed.setSource("foo\nbar\r\nbaz\n", url);
  QCOMPARE(docMixed.toPlainText(), docUnix.toPlainText());
}

QTEST_MAIN(Document);

#include "document.moc"
