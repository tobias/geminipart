// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminipart.h"
#include "geminiview.h"

#include <QTest>

#include <KPluginMetaData>
#include <QSignalSpy>
#include <QJsonDocument>
#include <KParts/BrowserExtension>

class PartTest : public QObject
{
    Q_OBJECT
private slots:
void testSetup();

  private:
    static KPluginMetaData metaData();
};

KPluginMetaData PartTest::metaData()
{
  QJsonObject data = QJsonDocument::fromJson(
    "{ \"KPlugin\": {\n"
    " \"Id\": \"geminipart\",\n"
    " \"Name\": \"Gemini\",\n"
    " \"Version\": \"0.1\"\n"
    "}\n}").object();
  return KPluginMetaData(data, QString());
}

void PartTest::testSetup()
{
  GeminiPart part(nullptr, nullptr, PartTest::metaData(), {});

  QSignalSpy spyCompleted(&part, qOverload<>(&KParts::ReadOnlyPart::completed));

  auto* ext = part.browserExtension();
  QSignalSpy spyOpenUrlNotify(ext, &KParts::BrowserExtension::openUrlNotify);

  const QUrl url("data:text/gemini,Hello world%0A");

  part.openUrl(url);

  QVERIFY(spyCompleted.wait());

  QVERIFY(spyOpenUrlNotify.isEmpty());

  QCOMPARE(part.view()->toPlainText(), "Hello world");
}


QTEST_MAIN(PartTest);

#include "parttest.moc"
