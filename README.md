# Gemini Viewer KPart

An KPart to display text/gemini files. Allows together with kio-gemini for
Konqueror to act as a Gemini client.
