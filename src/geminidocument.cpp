// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminidocument.h"
#include "gemini-debug.h"

#include <QTextCursor>
#include <QTextList>
#include <QFontDatabase>
#include <QPalette>
#include <QDebug>

static constexpr int headingMax = 3;

struct Formats {
  Formats()
  {
    link.setAnchor(true);
    QPalette palette;
    link.setForeground(palette.link());

    codeText.setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));

    codeBlock.setProperty(QTextFormat::BlockCodeFence, "Q");
    codeBlock.setNonBreakableLines(true);

    quoteText.setFontItalic(true);

    quoteBlock.setProperty(QTextFormat::BlockQuoteLevel, 1);
    quoteBlock.setProperty(QTextFormat::BlockMarker, "Q");

    for (int i = 0; i<headingMax; i++) {
      headingBlock[i].setHeadingLevel(i);

      headingText[i].setFontWeight(QFont::Bold);
      headingText[i].setProperty(QTextFormat::FontSizeAdjustment, headingMax - i);
    }
  }
  QTextBlockFormat block;

  QTextCharFormat link;

  QTextCharFormat text;

  QTextCharFormat codeText;
  QTextBlockFormat codeBlock;

  QTextCharFormat quoteText;
  QTextBlockFormat quoteBlock;

  QTextCharFormat headingText[headingMax];
  QTextBlockFormat headingBlock[headingMax];

};

Q_GLOBAL_STATIC(const Formats, formats);

GeminiDocument::GeminiDocument(QObject* parent)
: QTextDocument(parent)
{ }

const QString& GeminiDocument::source() const
{
  return mSource;
}

void removeFirstBlock(QTextDocument* doc)
{
  // removing the first block has bug
  // https://stackoverflow.com/a/24090727
  const auto count = doc->blockCount();

  QTextCursor c(doc);
  c.movePosition(QTextCursor::Start);
  c.select(QTextCursor::BlockUnderCursor);
  c.removeSelectedText();
  c.deleteChar();

  Q_ASSERT(doc->blockCount() + 1 == count);
}

void GeminiDocument::appendSourceLine(const QStringRef& line)
{
  const bool isFirstLine = isEmpty();

  if (line.startsWith("```")) {
    if (mParseState.isPreformating) {
      mParseState.isPreformating = false;
    } else {
      mParseState.isPreformating = true;
      mCursor.insertBlock(formats->codeBlock, formats->codeText);
    }
  } else if (mParseState.isPreformating) {
    mCursor.insertText(line.toString());
    mCursor.insertText("\n");
  } else if (line.startsWith("=>") && line.size()>3) {
    int start = mWhiteSpaceOptionalRegEx.match(line, 2).capturedEnd();
    const auto sep = mWhiteSpaceRegEx.match(line, start);

    QUrl url(line.mid(start,sep.capturedStart()-start).toString());
    if (url.isRelative()) {
      url = mUrl.resolved(url);
    }
    auto link = formats->link;
    link.setAnchorHref(url.toString());

    mCursor.insertBlock(formats->block, link);
    if (sep.hasMatch() && sep.capturedEnd() < line.size() -1) {
      mCursor.insertText(line.mid(sep.capturedEnd()).toString());
    } else {
      mCursor.insertText(url.toString());
    }
  } else if (!line.isEmpty() && line[0] == '#') {
    int level = 0;
    for (int c = 1; c<headingMax; c++) {
      if (c<line.size() && line[c] == '#') {
        level++;
      }
    }
    assert(level < headingMax);
    mCursor.insertBlock(formats->headingBlock[level], formats->headingText[level]);

    mCursor.insertText(line.mid(mWhiteSpaceOptionalRegEx.match(line,level+1).capturedEnd()).toString());
  } else if (line.startsWith("*")) {
    mCursor.insertBlock(formats->block, formats->text);
    mCursor.insertText(line.mid(1).trimmed().toString());
    if (!mParseState.currentList) {
      mParseState.currentList = mCursor.createList(QTextListFormat::ListDisc);
    } else {
      mParseState.currentList->add(mCursor.block());
    }
  } else if (line.startsWith(">")) {
    mCursor.insertBlock(formats->quoteBlock, formats->quoteText);
    mCursor.insertText(line.toString());
  } else {
    mCursor.insertBlock(formats->block, formats->text);
    mCursor.insertText(line.toString());
  }
  if (!line.startsWith("*")) {
    mParseState.currentList = nullptr;
  }

  // there is always a block; even in an empty document
  // remove it!
  if (isFirstLine && blockCount() == 2) {
    removeFirstBlock(this);
    qDebug(GEMINIPART) << "removing first block";
  }
}

void GeminiDocument::appendSource(const QString& source)
{
  qDebug(GEMINIPART) << "appendSource" << source;
  assert(mUrl.isValid());
  assert(!mCursor.isNull());

  // possibly incomplete line; remove and create anew
  if (mSourceNextLine < mSource.size()) {
    assert(mPositionLineStart >= 0);
    mCursor.setPosition(mPositionLineStart);
    mCursor.movePosition(QTextCursor::End, QTextCursor::KeepAnchor);
    qDebug(GEMINIPART) << "deleting line" << mCursor.selectedText();
    mCursor.removeSelectedText();
    mParseState = mParseStatePrevious;
  }

  mSource += source;

  QRegularExpression newLine("\r\n|\n");
  for (int i=mSourceNextLine; i<mSource.size(); ) {
    int lineEnd = mSource.indexOf(newLine, i);

    assert(-1 == lineEnd || lineEnd-i >= 0);
    auto line = lineEnd >= 0 ? mSource.midRef(i, lineEnd-i) : mSource.midRef(i);
    qDebug(GEMINIPART) << "processing line" << i  << " " << lineEnd << " " << line;

    if (-1 == lineEnd) {
      mPositionLineStart = mCursor.position();
      mParseStatePrevious = mParseState;
    } else {
      mPositionLineStart = -1;
    }

    appendSourceLine(line);

    if (-1 == lineEnd) {
      mSourceNextLine = i;
      i = mSource.size();
    } else {
      i = lineEnd + (mSource[lineEnd] == '\r' ? 2 : 1);
      mSourceNextLine = i;
    }

  }
}

void GeminiDocument::setUrl(const QUrl& url)
{ mUrl = url; }

void GeminiDocument::setSource(const QString& source, QUrl url)
{
  clear();
  setUrl(url);
  mCursor = QTextCursor(this),
  appendSource(source);
}

void GeminiDocument::clear()
{
  mSource.clear();
  mUrl.clear();
  QTextDocument::clear();
  mSourceNextLine = 0;
  mParseState.clear();
  mCursor = QTextCursor(this);
}

#include "geminidocument.moc"
