// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <KParts/BrowserExtension>

class KActionCollection;
class GeminiPart;


class GeminiBrowserExtension : public KParts::BrowserExtension
{
  Q_OBJECT

  public:
    explicit GeminiBrowserExtension(GeminiPart* part);

    int xOffset() override;
    int yOffset() override;

  public slots:
    void copy();
    void requestContextMenu(const QPoint&, const QUrl&, bool);

    void infoMessage(KJob *job, const QString &plain, const QString&);

  private:
    GeminiPart* m_part;
    KActionCollection* m_contextMenuActionCollection;
    KParts::BrowserExtension::ActionGroupMap actionGroups;
};

