// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QTextBrowser>
#include <QObject>

#include "geminibrowserextension.h"

class GeminiView : public QTextBrowser
{
  Q_OBJECT
  public:
    GeminiView(QWidget* parent);
    using QTextEdit::contextMenuEvent;

    void setScrollPosition(const QPoint& offset);
    int scrollPositionX() const;
    int scrollPositionY() const;

  public slots:
    void zoomIn();
    void zoomOut();
    void resetZoom();
  signals:
    void contextMenuRequested(QPoint globalPos, const QUrl& linkUrl,
        bool hasSelection);

    void linkMiddleClicked(QUrl link);

  protected:
    void contextMenuEvent(QContextMenuEvent* event) override;
    void mouseReleaseEvent(QMouseEvent* event) override;

  private:
    int mZoomStep = 0;
};
