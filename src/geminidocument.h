// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <QTextDocument>
#include <QTextCursor>
#include <QRegularExpression>

class QTextList;

class GeminiDocument : public QTextDocument
{
  public:
    GeminiDocument(QObject* parent=nullptr);

    void appendSource(const QString& source);
    void setSource(const QString& source, QUrl url);

    void clear() override;

    void setUrl(const QUrl& url);

    const QString& source() const;

  protected:
    QString mSource;
    int mSourceNextLine = 0;
    QUrl mUrl;
    QTextCursor mCursor{this};
    int mPositionLineStart = -1;

    const QRegularExpression mWhiteSpaceRegEx{"\\s+"};
    const QRegularExpression mWhiteSpaceOptionalRegEx{"\\s*"};

    struct ParseState {
      bool isPreformating = false;
      QTextList* currentList = nullptr;
      void clear() { isPreformating = false; currentList = nullptr; };
    } mParseState;
    ParseState mParseStatePrevious;

    void appendSourceLine(const QStringRef& line);
};
