// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminipart.h"

#include <ksslinfodialog.h>
#include <KPluginMetaData>
#include <KParts/OpenUrlArguments>
#include <KLocalizedString>
#include <KAboutData>
#include <QApplication>
#include <QFile>
#include <QInputDialog>
#include <KIO/TransferJob>
#include <KIO/OpenUrlJob>
#include <KIO/JobUiDelegate>
#include <KParts/BrowserRun>

#include <QFileDialog>

#include "geminiview.h"
#include "geminibrowserextension.h"
#include "geminidocument.h"

#include <QTextDecoder>

#include "gemini-debug.h"


enum PageSecurity { PageUnencrypted, PageEncrypted, PageMixed };

GeminiPart::GeminiPart( QWidget *parentWidget, QObject *parent, const KPluginMetaData& metaData, const QVariantList&)
  : KParts::ReadOnlyPart(parent)
  , mDecode()
  , mDocument(new GeminiDocument(this))
  , mMainWidget(new GeminiView(parentWidget))
  , mBrowserExtension(new GeminiBrowserExtension(this))
  , mCollection(new KActionCollection(mMainWidget))
{
  setMetaData(metaData);

  mMainWidget->setDocument(mDocument);
  mMainWidget->setObjectName(QStringLiteral("geminipart"));
  setWidget(mMainWidget);

  initActions();

  connect(view(), &QTextEdit::copyAvailable,
          this, &GeminiPart::copyAvailable);
  connect(view(), &QTextEdit::selectionChanged,
          this, &GeminiPart::selectionChanged);

  setXMLFile("geminipart.rc");
}

GeminiPart::~GeminiPart()
{
  if (mJob) {
    mJob->kill();
  }
}

bool GeminiPart::openUrl(const QUrl &url)
{
  qDebug(GEMINIPART) << "openurl " << url;
  if (!url.isValid()) {
    return false;
  }
  setUrl(url);

  mDocument->clear();
  mDocument->setUrl(url);

  Q_EMIT setWindowCaption( url.toDisplayString() );
  /*
     if (url.isLocalFile()) {
     setLocalFilePath(url.url());
     return openFile();
     }
     */

  if (mJob) {
    mJob->kill();
  }

  Q_EMIT setWindowCaption( url.toDisplayString() );

  mJob = KIO::get(url, arguments().reload() ? KIO::Reload : KIO::NoReload, KIO::HideProgressInfo);
  mJob->addMetaData("inputSupported", "true");
  qDebug(GEMINIPART) << "set outgoing metadata" << mJob->outgoingMetaData();
  connect(mJob, &KIO::TransferJob::mimeTypeFound, this, &GeminiPart::mimeType);
  connect(mJob, &KIO::TransferJob::data, this, &GeminiPart::data);
  connect(mJob, &KIO::Job::finished, this, &GeminiPart::jobFinished);
  connect(mJob, &KIO::Job::result, this, &GeminiPart::result);
  connect(mJob, &KJob::infoMessage, mBrowserExtension,
          &GeminiBrowserExtension::infoMessage);
  mJob->setUiDelegate(new KIO::JobUiDelegate(KJobUiDelegate::AutoHandlingEnabled, view()));
  mJob->start();

  emit started(mJob);

  return true;
}

void GeminiPart::jobFinished(KJob* job) {
  Q_EMIT completed();

  if (mJob) {
    Q_ASSERT(job == mJob);
    mJob = nullptr;
  }
}

void GeminiPart::restoreScrollPosition()
{
    const KParts::OpenUrlArguments args(arguments());
    view()->setScrollPosition({args.xOffset(), args.yOffset()});
    qDebug(GEMINIPART) << "scroll" << mBrowserExtension->xOffset() << " " << mBrowserExtension->yOffset();
}

bool GeminiPart::closeUrl()
{
  if (mJob) {
    mJob->kill();
  }

  return ReadOnlyPart::closeUrl();
}

void GeminiPart::data(KIO::Job*, const QByteArray & data )
{
  mDocument->appendSource(mDecode->toUnicode(data));
}

void GeminiPart::result(KJob* kjob)
{
  qDebug(GEMINIPART) << "loading done";

  auto job = qobject_cast<KIO::Job*>(kjob);
  if (job && !job->queryMetaData("input").isNull()) {
    const auto prompt = job->queryMetaData("input");
    const auto text = QInputDialog::getText(mMainWidget, "Gemini Prompt", prompt);
    if (!text.isEmpty()) {
      auto url = this->url();
      url.setQuery(text);
      openUrl(url);
    }
    return;
  }

  const KParts::OpenUrlArguments args(arguments());
  restoreScrollPosition();
}

void GeminiPart::mimeType(KIO::Job* job, const QString& /*mimeType*/)
{
  mMetaData = job->metaData();

  qDebug(GEMINIPART) << "meta" << job->metaData();
  auto charset = job->metaData().value("charset","UTF-8").toUtf8();
  auto codec = QTextCodec::codecForName(charset);
  qDebug(GEMINIPART) << "charset: " << charset;
  if (codec) {
    mDecode.reset(codec->makeDecoder());
  } else {
    auto c = QTextCodec::QTextCodec::codecForName("UTF-8");
    Q_ASSERT(c);
    mDecode.reset(c->makeDecoder());
  }

  emit mBrowserExtension->setPageSecurity(PageEncrypted);
}

void GeminiPart::saveDocument()
{
  QString name = url().fileName().isEmpty() ? "index.gmi" :  url().fileName();

  QFileDialog dialog(view(), "Save As");
  dialog.setAcceptMode(QFileDialog::AcceptSave);
  dialog.setMimeTypeFilters({"text/gemini"});
  dialog.selectFile(name);

  if (dialog.exec() == QDialog::Accepted) {
    auto destUrl = dialog.selectedUrls().value(0);
    KParts::BrowserRun::saveUrlUsingKIO(url(), destUrl, view(), {});
  }
}

void GeminiPart::initActions()
{
  actionCollection()->addAction(KStandardAction::SaveAs, "saveDocument", this, SLOT(saveDocument()));

  QAction* action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-in")), i18nc("zoom in action", "Zoom In"), this);
  actionCollection()->addAction(QStringLiteral("zoomIn"), action);
  actionCollection()->setDefaultShortcuts(action, QList<QKeySequence> () << QKeySequence(QStringLiteral("CTRL++")) << QKeySequence(QStringLiteral("CTRL+=")));
  connect(action, &QAction::triggered, view(), &GeminiView::zoomIn);

  action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-out")), i18nc("zoom out action", "Zoom Out"), this);
  actionCollection()->addAction(QStringLiteral("zoomOut"), action);
  actionCollection()->setDefaultShortcuts(action, QList<QKeySequence> () << QKeySequence(QStringLiteral("CTRL+-")) << QKeySequence(QStringLiteral("CTRL+_")));
  connect(action, &QAction::triggered, view(), &GeminiView::zoomOut);

  action = new QAction(QIcon::fromTheme(QStringLiteral("zoom-original")), i18nc("reset zoom action", "Actual Size"), this);
  actionCollection()->addAction(QStringLiteral("zoomNormal"), action);
  actionCollection()->setDefaultShortcut(action, QKeySequence(QStringLiteral("CTRL+0")));
  connect(action, &QAction::triggered, view(), &GeminiView::resetZoom);

  action = new QAction(i18n("View Do&cument Source"), this);
  actionCollection()->addAction(QStringLiteral("viewDocumentSource"), action);
  actionCollection()->setDefaultShortcut(action, QKeySequence(Qt::CTRL | Qt::Key_U));
  connect(action, &QAction::triggered, this, [&]() {
    KIO::OpenUrlJob* job = new KIO::OpenUrlJob(url(), "text/plain");
    job->setUiDelegate(new KIO::JobUiDelegate(KJobUiDelegate::AutoHandlingEnabled, view()));
    job->start(); });

  action = new QAction("SSL", this);
  actionCollection()->addAction(QStringLiteral("security"), action);
  connect(action, &QAction::triggered, this, &GeminiPart::showSecurity);

  //action = KStandardAction::create(KStandardAction::Find, this,
  //    &WebEnginePart::slotShowSearchBar, actionCollection());
  //
  //  action = new QAction(QIcon::fromTheme(QStringLiteral("document-print-preview")), i18n("Print Preview"), this);
  //   actionCollection()->addAction(QStringLiteral("printPreview"), action);
  //   connect(action, &QAction::triggered, mBrowserExtension, &WebEngineBrowserExtension::slotPrintPreview);

}

void GeminiPart::showSecurity()
{
  qDebug(GEMINIPART) << "ssl: " << mMetaData;
  KSslInfoDialog *kid = new KSslInfoDialog(nullptr);

  QStringList sl = mMetaData["ssl_peer_chain"].split('\x01', Qt::SkipEmptyParts);
  QList<QSslCertificate> certChain;
  foreach (const QString &s, sl) {
    certChain.append(QSslCertificate(s.toLatin1()));
  }

  kid->setSslInfo(certChain,
                  mMetaData["ssl_peer_ip"],
                  this->url().host(),
                  mMetaData["ssl_protocol_version"],
                  mMetaData["ssl_chipher"],
                  mMetaData["ssl_cipher_used_bits"].toInt(),
                  mMetaData["ssl_cipher_bits"].toInt(),
                  KSslInfoDialog::certificateErrorsFromString(mMetaData["ssl_cert_errors"]));

  kid->exec();
}

void GeminiPart::copyAvailable(bool yes)
{
  emit mBrowserExtension->enableAction("copy", yes);
  QString selection = view()->textCursor().selectedText();
  emit mBrowserExtension->selectionInfo(selection);
}
void GeminiPart::selectionChanged()
{
  QString selection = view()->textCursor().selectedText();
  emit mBrowserExtension->selectionInfo(selection);
}

void GeminiPart::copySelection()
{
  view()->copy();
}


#include "geminipart.moc"
