// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminiview.h"

#include <QScrollBar>
#include <QTextCursor>
#include <QContextMenuEvent>


GeminiView::GeminiView(QWidget* parent)
  : QTextBrowser(parent)
{
  setOpenLinks(false);
}

void GeminiView::contextMenuEvent(QContextMenuEvent* event)
{
  const QUrl link(anchorAt(event->pos()));
  const bool hasSelection = !link.isValid() && textCursor().hasSelection();
  event->accept();

  Q_EMIT contextMenuRequested(event->globalPos(), link, hasSelection);
}

void GeminiView::mouseReleaseEvent(QMouseEvent* event)
{
  if (event->button() == Qt::MiddleButton && !textCursor().hasSelection()) {
    const QUrl link(anchorAt(event->pos()));
    if (link.isValid()) {
      Q_EMIT linkMiddleClicked(link);
    }
  }
  QTextBrowser::mouseReleaseEvent(event);
}

void GeminiView::setScrollPosition(const QPoint& offset)
{
  verticalScrollBar()->setValue(offset.x());
  horizontalScrollBar()->setValue(offset.y());
}

int GeminiView::scrollPositionX() const
{ return verticalScrollBar()->value(); }

int GeminiView::scrollPositionY() const
{ return horizontalScrollBar()->value(); }

void GeminiView::zoomIn()
{
  QTextEdit::zoomIn(1);
  mZoomStep++;
}

void GeminiView::zoomOut()
{
  QTextEdit::zoomOut(1);
  mZoomStep--;
}

void GeminiView::resetZoom()
{
  QTextEdit::zoomIn(-mZoomStep);

  mZoomStep = 0;
}

#include "geminiview.moc"
