// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#pragma once

#include <kparts_version.h>
#include <KParts/ReadOnlyPart>
#include <QLoggingCategory>
#include <QScopedPointer>
#include <KActionCollection>
#include <KIO/MetaData>

class GeminiView;
class QTextDocument;
class GeminiBrowserExtension;
class GeminiDocument;

Q_DECLARE_LOGGING_CATEGORY(KGEMINIPART)

  namespace KIO {
    class TransferJob;
  }

class GeminiPart : public KParts::ReadOnlyPart
{
  Q_OBJECT

  public:
    GeminiPart(QWidget *parentWidget, QObject *parent,
        const KPluginMetaData& metaData, const QVariantList& args);

    ~GeminiPart() override;

    GeminiView* view()
    { return mMainWidget; }


    //bool openFile() override;
    bool openUrl(const QUrl& url) override;

    void copySelection();

    GeminiBrowserExtension* extension()
    { return mBrowserExtension; }

    bool closeUrl() override;

  protected:
    void initActions();

    void restoreScrollPosition();

    QScopedPointer<QTextDecoder> mDecode;

    GeminiDocument* mDocument;
    GeminiView* mMainWidget;

    KIO::TransferJob* mJob = nullptr;

    GeminiBrowserExtension* mBrowserExtension;

    KActionCollection mCollection;
    KIO::MetaData mMetaData;

  protected slots:
    void data(KIO::Job *,const QByteArray &);
    void mimeType(KIO::Job*, const QString& mimeType);
    void jobFinished(KJob *);
    void result(KJob *);
    void showSecurity();
    void saveDocument();

    void copyAvailable(bool);
    void selectionChanged();
};
