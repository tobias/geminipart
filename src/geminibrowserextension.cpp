// SPDX-FileCopyrightText: 2021 Tobias Rautenkranz <mail@tobias.rautenkranz.ch>
//
// SPDX-License-Identifier: LGPL-2.0-or-later

#include "geminibrowserextension.h"

#include "geminipart.h"
#include "geminiview.h"

#include "gemini-debug.h"

#include <QScrollBar>

#include <KActionCollection>
#include <KParts/BrowserRun>
#include <KStringHandler>
#include <QTextBrowser>
#include <KFileItem>
#include <QContextMenuEvent>
#include <QMimeDatabase>
#include <QMimeData>
#include <QApplication>
#include <QClipboard>
#include <KUriFilter>
#include <klocalizedstring.h>

enum PageSecurity { PageUnencrypted, PageEncrypted, PageMixed };

GeminiBrowserExtension::GeminiBrowserExtension(GeminiPart* part)
  : KParts::BrowserExtension(part)
  , m_part(part)
    , m_contextMenuActionCollection(new KActionCollection(this))
{
  setURLDropHandlingEnabled(true);
  Q_EMIT enableAction("copy", true);
  Q_EMIT enableAction("paste", true);

  connect(part->view(), &QTextBrowser::anchorClicked,
          this,
          [&](const QUrl &link) { qDebug(GEMINIPART) << "navigate" << link;
            Q_EMIT openUrlRequest(link);
          });

  connect(part->view(), &GeminiView::linkMiddleClicked,
          this,
          [&](const QUrl &link) { qDebug(GEMINIPART) << "new Tab" << link;
            KParts::BrowserArguments browserArguments;
            browserArguments.setNewTab(true);
            Q_EMIT openUrlRequest(link, KParts::OpenUrlArguments(), browserArguments);
          });

  connect(part->view(), SIGNAL(highlighted(QUrl)),
          this, SLOT(mouseOverInfo(KFileItem)));
  connect(part->view(), QOverload<const QUrl &>::of(&QTextBrowser::highlighted),
          this,
          [&](const QUrl &link){
            KFileItem fileItem;
            if (link.isValid()) {
              fileItem = KFileItem(link, QString(), KFileItem::Unknown);
              Q_EMIT m_part->setStatusBarText(link.toString());
            } else {
              Q_EMIT m_part->setStatusBarText(QString());
            }
            Q_EMIT mouseOverInfo(fileItem);
          });


  connect(part->view(), &GeminiView::contextMenuRequested,
          this, &GeminiBrowserExtension::requestContextMenu);

  emit setPageSecurity(PageEncrypted);
}

void GeminiBrowserExtension::requestContextMenu(const QPoint& pos, const QUrl& hlink, bool hasSelection)
{
  KParts::BrowserExtension::PopupFlags flags = KParts::BrowserExtension::DefaultPopupItems;
  flags |= KParts::BrowserExtension::ShowBookmark | KParts::BrowserExtension::ShowReload;

  KParts::OpenUrlArguments args;
  KParts::BrowserArguments browserArgs;

  qDebug(GEMINIPART) << "context menu browser" << hasSelection;

  QUrl link = hlink;

  if (link.isValid()) {
    flags |= KParts::BrowserExtension::IsLink;
    flags |= KParts::BrowserExtension::ShowBookmark;
    flags |=  KParts::BrowserExtension::ShowUrlOperations;

    QList<QAction*> linkActions;

    QAction* action = new QAction(i18n("Copy Link"), m_part);
    connect(action, &QAction::triggered, this, [&] {
      auto* data = new QMimeData;
      data->setUrls({link});
      QApplication::clipboard()->setMimeData(data, QClipboard::Clipboard);
    });
    m_contextMenuActionCollection->addAction(QStringLiteral("copylinkurl"), action);
    linkActions.append(action);

    action = new QAction(i18n("&Save Link As..."), this);
    m_contextMenuActionCollection->addAction(QStringLiteral("savelinkas"), action);
    connect(action, &QAction::triggered, this, [&](bool){
      KParts::BrowserRun::saveUrl(link, link.path(), m_part->view(), KParts::OpenUrlArguments());
    });
    linkActions.append(action);

    actionGroups.insert(QStringLiteral("linkactions"), linkActions);
  } else {
    link = m_part->url();
    if (hasSelection) {
      flags =  KParts::BrowserExtension::ShowTextSelectionItems;

      QList<QAction*> editActions;

      /*
      auto copy = KStandardAction::copy(m_part);
      copy->setText("&Copy Text");
      copy->setEnabled(true);
      connect(m_part->view(), &QTextEdit::copyAvailable,
              copy, &QAction::setEnabled);
      connect(copy, &QAction::triggered, m_part, &GeminiPart::copySelection);

      editActions.append(copy);
      */

      auto text = m_part->view()->textCursor().selectedText();
      KUriFilterData data(text);
      //data.setDefaultUrlScheme("gemini");

      QString searchProvider = "geminispace";
      // QStringList alternateProviders;     alternateProviders << "gus" << "google" << "google_groups" << "google_news" << "webster" << "dmoz" << "wikipedia";
      //data.setAlternateSearchProviders(alternateProviders);
      data.setAlternateDefaultSearchProvider(searchProvider);

      //data.setSearchFilteringOptions(KUriFilterData::RetrievePreferredSearchProvidersOnly);
      qDebug(GEMINIPART) << "providers" << data.preferredSearchProviders() << data.searchProvider();

      //bool filtered = KUriFilter::self()->filterSearchUri(data, KUriFilter::WebShortcutFilter);
      bool filtered = KUriFilter::self()->filterSearchUri(data, KUriFilter::NormalTextFilter);
      if (filtered) {
        auto search = new QAction(i18n("Search for '%1' with %2",
                                       KStringHandler::rsqueeze(text, 21), searchProvider), m_part);
        search->setData(QUrl(data.uri()));
        connect(search, &QAction::triggered, this, [&](bool)
                {
                  qDebug(GEMINIPART) << "search" << data.uri() << data.uriType();
                  QUrl url = data.uri();
                  KParts::BrowserArguments browserArgs;
                  browserArgs.frameName = "_blank";
                  emit m_part->extension()->openUrlRequest(url, KParts::OpenUrlArguments(), browserArgs);
                });
        //editActions.addAction("defaultSearchProvider", action);
        editActions.append(search);
      }

      actionGroups.insert(QStringLiteral("editactions"), editActions);

    } else {
      flags |=  KParts::BrowserExtension::ShowNavigationItems;
    }
  }

  QString mimetype = QLatin1String("text/gemini");
  if (!link.isEmpty() && link.isLocalFile()) {
    QMimeDatabase db;
    mimetype = db.mimeTypeForUrl(link).name();
  }
  args.setMimeType(mimetype);

  Q_EMIT popupMenu(pos, link, static_cast< mode_t >(-1),
                   args, browserArgs, flags, actionGroups);
}

void GeminiBrowserExtension::infoMessage(KJob*, const QString& plain, const QString&)
{
  qDebug(GEMINIPART) << "infomessage" << plain;
  emit BrowserExtension::infoMessage(plain);
}


void GeminiBrowserExtension::copy()
{
  // m_part->view()->copySelection();
}

int GeminiBrowserExtension::xOffset()
{ return m_part->view()->scrollPositionX(); }
int GeminiBrowserExtension::yOffset()
{ return m_part->view()->scrollPositionY(); }


/*
   void GeminiBrowserExtension::printPreview()
   {
   QPrintPreviewDialog dialog(m_part->view());
   connect(&dialog, &QPrintPreviewDialog::paintRequested, [=](QPrinter *p) {

   });
   }
   */

#include "geminibrowserextension.moc"
