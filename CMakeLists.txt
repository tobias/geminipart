project(geminipart)

cmake_minimum_required(VERSION 3.5)

set(QT_MIN_VERSION "5.4.0")
set(KF5_MIN_VERSION "5.16.0")

set (CMAKE_CXX_STANDARD 14)

find_package(ECM ${KF5_MIN_VERSION} REQUIRED NO_MODULE)
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${ECM_MODULE_PATH} ${ECM_KDE_MODULE_DIR})

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMAddTests)

include(ECMQtDeclareLoggingCategory)

include(FeatureSummary)

find_package(Qt5 ${QT_MIN_VERSION} REQUIRED Widgets)
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED I18n Parts)

find_package(SharedMimeInfo REQUIRED)

add_subdirectory(src)
add_subdirectory(integration)


ki18n_install(po)

add_subdirectory(autotests)
